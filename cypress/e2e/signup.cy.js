import Signup from '../pom/signup';

describe('Signup with POM', () => {
  const signup = new Signup();

  beforeEach(() => {
    signup.visit();
  });

  it('should signup with valid credentials', () => {
    signup.fillUsername('ljdiwu');
    signup.fillEmail('ljuj@gmail.com');
    signup.fillPassword('11111');
    signup.submit();

    // Assert that the "Your Feed" is visible after signing up
    cy.contains('Your Feed').should('be.visible');
  });

  it('should show error for invalid email format', () => {
    signup.fillUsername('lulu');
    signup.fillEmail('pmail.com'); // Invalid email format
    signup.fillPassword('11111');
    signup.submit();

    // Should show error message
    //cy.get('.error-messages').should('contain', 'email must be a valid email');
  });

  it('should show error for empty email', () => {
    signup.fillUsername('lulu');
    // Not filling the email
    signup.fillPassword('11111');
    signup.submit();

    // Should show error message
   // cy.get('.error-messages').should('contain', 'email cannot be empty');
  });

  it('should show error for empty password', () => {
    signup.fillUsername('lulu');
    signup.fillEmail('yuni@gmail.com');
    // Not filling the password
    signup.submit();

    // Should show error message
    //cy.get('.error-messages').should('contain', 'password cannot be empty');
  });

  it('should show error for empty username', () => {
    // Not filling the username
    signup.fillEmail('uu@.com');
    signup.fillPassword('11111');
    signup.submit();

    // Should show error message
    //cy.get('.error-messages').should('contain', 'username cannot be empty');
  });
});
