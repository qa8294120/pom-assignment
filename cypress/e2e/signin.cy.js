import Signin from '../pom/signin';

describe('Signin with POM', () => {
  const signin = new Signin();

  beforeEach(() => {
    signin.visit();
  });

  it('should log in with valid credentials', () => {
    signin.fillEmail('yuni@gmail.com');
    signin.fillPassword('11111');
    signin.submit();

    // Assert that the "Your Feed" is visible after logging in
    cy.contains('Your Feed').should('be.visible');
  });

  it('should show error for invalid credentials', () => {
    signin.fillEmail('p@gmail.com');
    signin.fillPassword('wrongpassword');
    signin.submit();

    // Should show error message
    cy.contains('email or password is invalid').should('be.visible');
  });

  it('should show error for empty email', () => {
    // Not filling the email
    signin.fillPassword('11111');
    signin.submit();

    // Should show error message
   // cy.get('.error-messages').should('contain', 'email must be a valid email');
  });

  it('should show error for null password', () => {
    signin.fillEmail('yuni@gmail.com');
    // Not filling the password
    signin.submit();

    // Should show error message
    //cy.get('.error-messages').should('contain', 'password cannot be empty');
  });

  it('should show error for invalid password', () => {
    signin.fillEmail('yuni@gmail.com');
    signin.fillPassword('short');
    signin.submit();

    // Should show error message
    //cy.get('.error-messages').should('contain', 'password is too short');
  });
});
